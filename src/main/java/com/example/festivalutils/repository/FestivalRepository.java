package com.example.festivalutils.repository;

import com.example.festivalutils.entity.Festival;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;

public interface FestivalRepository extends JpaRepository<Festival, Long> {

  Optional<Festival> findByName(String name);

}
