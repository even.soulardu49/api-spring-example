package com.example.festivalutils.controller;

import com.example.festivalutils.entity.Festival;
import com.example.festivalutils.repository.FestivalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/api/festival")
public class FestivalController {

  final FestivalRepository festivalRepository;

  @Autowired
  public FestivalController(FestivalRepository festivalRepository) {
    this.festivalRepository = festivalRepository;
  }

  @GetMapping("")
  public List<Festival> getFestivaux(){
    List<Festival> festivaux = new ArrayList<>();

    try{
      festivaux = festivalRepository.findAll();
    }catch (Exception e){
      System.out.println(e);
    }

    return festivaux;
  }

  @PostMapping("")
  public ResponseEntity<?> createFestival(String name) throws IOException {
    try{
      festivalRepository.save(new Festival(name));
      return new ResponseEntity<>(
          "C'est ok",
          HttpStatus.OK);
    }catch (Exception e){
      System.out.println(e);
      return new ResponseEntity<>(
          "C'est pas ok",
          HttpStatus.BAD_REQUEST);
    }
  }

}

