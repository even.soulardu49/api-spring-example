package com.example.festivalutils.controller;

import com.example.festivalutils.entity.admin.Admin;
import com.example.festivalutils.entity.admin.AdminAuthImpl;
import com.example.festivalutils.payload.response.JwtResponse;
import com.example.festivalutils.repository.AdminRepository;
import com.example.festivalutils.security.jwt.JwtUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.*;
import org.springframework.security.crypto.password.PasswordEncoder;

import java.io.IOException;

@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(path = "/api/admin")
public class AdminController {

    final AdminRepository adminRepository;

    final PasswordEncoder encoder;

    final AuthenticationManager authenticationManager;

    final JwtUtils jwtUtils;

    @Autowired
    public AdminController(
            AdminRepository adminRepository,
            PasswordEncoder encoder,
            @Qualifier("admin_manager") AuthenticationManager authenticationManager,
            JwtUtils jwtUtils
    ) {
        this.adminRepository = adminRepository;
        this.authenticationManager = authenticationManager;
        this.encoder = encoder;
        this.jwtUtils = jwtUtils;
    }

    @PostMapping("/authenticate")
    public ResponseEntity<?> authenticate(String email, String password) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(email, password));

        SecurityContextHolder.getContext().setAuthentication(authentication);
        AdminAuthImpl adminImpl = (AdminAuthImpl) authentication.getPrincipal();

        String jwt = jwtUtils.generateJwtToken(adminImpl.getUsername());

        return ResponseEntity.ok(new JwtResponse(jwt));
    }



    @PostMapping("/register")
    public ResponseEntity<?> register(String name, String email, String password) {
        //TODO: check si user n'existe pas

        try {
            Admin newAdmin = new Admin(
                    name,
                    email,
                    encoder.encode(password)
            );
            adminRepository.save(newAdmin);
            return new ResponseEntity<>(
                    "C'est ok",
                    HttpStatus.OK);
        } catch (Exception e) {
            System.out.println(e);
            return new ResponseEntity<>(
                    "C'est pas ok",
                    HttpStatus.BAD_REQUEST);
        }
    }

}